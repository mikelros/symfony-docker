# SYMFONY DOCKER #

A boilerplate for Symfony projects that run on Docker. With: PHP 7.1 FPM, MySQL and NGINX.

## Steps ##

* Clone repository
* Start your new Symfony app in the same folder
* Run Docker Composer and composer
* Config your parameters.yml

### Clone repository ###

* git clone https://mikelros@bitbucket.org/mikelros/symfony-docker.git

### Start Symfony app ###

* symfony new app

### Docker Compose and Composer ###

1. docker-compose build
2. docker-compose up [-d]
3. docker exec -it [php-container] bash
4. cd /app
5. composer install
6. composer update

### Config parameters ###

* database_host: db
* database_port: 3306
* database_name: db